package com.company.persistence;

import com.company.entity.Party;

public interface PartyDao {


	public Party showPartyDetail(String partyId) throws Exception;
	public String showResults() throws Exception;
	public boolean partyLogin(String partyId,String partyPassword) throws Exception;
}
