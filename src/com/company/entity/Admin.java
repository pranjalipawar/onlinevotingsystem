package com.company.entity;

public class Admin {
	
	private String adminId;
	private String adminPassword;
	private int hasDeclared;
	private int hasSarted;
	private String partywon;
	@Override
	public String toString() {
		return "Admin [adminId=" + adminId + ", adminPassword=" + adminPassword + ", hasDeclared=" + hasDeclared
				+ ", hasSarted=" + hasSarted + ", partywon=" + partywon + "]";
	}
	public String getAdminId() {
		return adminId;
	}
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}
	public String getAdminPassword() {
		return adminPassword;
	}
	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
	public int getHasDeclared() {
		return hasDeclared;
	}
	public void setHasDeclared(int hasDeclared) {
		this.hasDeclared = hasDeclared;
	}
	public int getHasSarted() {
		return hasSarted;
	}
	public void setHasSarted(int hasSarted) {
		this.hasSarted = hasSarted;
	}
	public String getPartywon() {
		return partywon;
	}
	public void setPartywon(String partywon) {
		this.partywon = partywon;
	}
	public Admin(String adminId, String adminPassword, int hasDeclared, int hasSarted, String partywon) {
		super();
		this.adminId = adminId;
		this.adminPassword = adminPassword;
		this.hasDeclared = hasDeclared;
		this.hasSarted = hasSarted;
		this.partywon = partywon;
	}
	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}

}
