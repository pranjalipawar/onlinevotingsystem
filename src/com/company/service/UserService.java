package com.company.service;

import com.company.entity.Voters;
import com.company.persistence.*;


public class UserService {
	public int voteToAParty(String partyId,String userId) throws Exception{
		return new UserDaoImpl().voteToAParty(partyId, userId);
	}
	public int registerUserDetails(Voters voter) throws Exception{
		
		return new UserDaoImpl().registerUserDetails(voter);
		
	}
	public int loginUser(String userId,String password) throws Exception{
		return new UserDaoImpl().loginUser(userId, password);
	}
	
public int deleteVoters(String userId) throws Exception{
		
		return new UserDaoImpl().deleteVoters(userId);
	}
	
	public int updateVoters(String userId,String newPassword) throws Exception{
		return new UserDaoImpl().updateVoters(userId, newPassword);
	}
	
	public void showResults() throws Exception{
		
		 new UserDaoImpl().showResults();
	}
}

