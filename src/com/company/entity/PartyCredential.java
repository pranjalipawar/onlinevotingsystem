package com.company.entity;

public class PartyCredential {
	
	private String partyId;
	private String partyPassword;
	public PartyCredential() {
		
	}
	public PartyCredential(String partyId, String partyPassword) {
		this.partyId = partyId;
		this.partyPassword = partyPassword;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getPartyPassword() {
		return partyPassword;
	}
	public void setPartyPassword(String partyPassword) {
		this.partyPassword = partyPassword;
	}
	
	
	
	

}
