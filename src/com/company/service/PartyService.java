package com.company.service;

import com.company.entity.Party;
import com.company.persistence.AdminDaoImpl;
import com.company.persistence.PartyDaoImpl;

public class PartyService {
	public Party showPartyDetail(String partyId) throws Exception {
		return new PartyDaoImpl().showPartyDetail(partyId);
	}

	public String getResults() throws Exception {

		return new PartyDaoImpl().showResults();
	}
	
	public boolean partyLogin(String partyId,String partyPassword) throws Exception{
		
		return new PartyDaoImpl().partyLogin(partyId,partyPassword);
	}
}

