package com.company.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.company.entity.Party;
import com.company.entity.PartyCredential;
import com.company.ui.ConnectionClass;

public class PartyDaoImpl implements PartyDao {

	static PreparedStatement prepareStatement = null;
	static ResultSet rs = null;

	@Override
	public Party showPartyDetail(String partyId) throws Exception {
		String query = "select * from party where partyId =?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, partyId);
		rs = prepareStatement.executeQuery();
		Party p = new Party();
		if (rs.next()) {
			PartyCredential pc = new PartyCredential();
			pc.setPartyId(rs.getString(1));
			p.setPartyId(rs.getString(1));
			p.setPartyName(rs.getString(2));
			p.setCandidateName(rs.getString(3));
			p.setNoOfVotes(rs.getInt(4));

			p.setpCredentials(pc);
		}

		return p;

	}

	@Override
	public String showResults() throws Exception {
		String query = "select hasdeclared from admin";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		rs = prepareStatement.executeQuery();
		if (rs.next()) {

			if (rs.getInt(1) == 1) {

				String query1 = "select partyName,noOfVotes from party where noOfVotes=(select max(noOfVotes) from party) limit 2";

				prepareStatement = new ConnectionClass().connection().prepareStatement(query1);
				rs = prepareStatement.executeQuery();

				int noOfvotes = 0;
				int noOfvotes1 = 0;
				String name1 = "";
				String name2 = "";

				int j = 0;
				while (rs.next()) {

					if (j == 0) {
						noOfvotes = rs.getInt(2);
						name1 = rs.getString(1);

						
					} else {
						noOfvotes1 = rs.getInt(2);
						name2 = rs.getString(1);
						
					}
					j++;
				}
				if (noOfvotes == noOfvotes1) {

					return "Election Tied. Admin Will Restart the Election";
				}

				else {
					return name1 + " won the election with " + noOfvotes + " Total number of votes";
				}
			}

		}

		return "Result Not Declared";
	}

	@Override
	public boolean partyLogin(String partyId, String partyPassword) throws Exception {

		String query = "select * from partyCredentials where partyId =?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, partyId);
		rs = prepareStatement.executeQuery();
		if (rs.next()) {
			if (rs.getString(1).equals(partyId) && rs.getString(2).equals(partyPassword)) {
				return true;
			}
		}
		return false;
	}

}
