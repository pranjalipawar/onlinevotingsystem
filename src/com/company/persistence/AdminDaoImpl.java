package com.company.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.company.entity.Party;
import com.company.entity.PartyCredential;
import com.company.entity.Voters;
import com.company.entity.VotersCredentials;
import com.company.ui.ConnectionClass;

public class AdminDaoImpl implements AdminDao {

	static PreparedStatement prepareStatement = null;
	static ResultSet rs = null;

	@Override
	public int adminLogin(String adminId, String adminPassword) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void showVotersDetails() throws Exception {
		// TODO Auto-generated method stub
		

	}
	
	public List<Party> showAllParty() throws Exception {
		List<Party> partyList = new ArrayList<>();
		String query = "select * from party";
		Statement st = new ConnectionClass().connection().createStatement();
		st.executeQuery(query);
		ResultSet rs = st.getResultSet();
		while (rs.next()) {
			Party p = new Party(rs.getString(1), rs.getString(2), rs.getString(3), rs.getInt(4));
			partyList.add(p);
		}
		
		
		return partyList;
	}

	@Override
	public int addParty(Party p) throws Exception {
		// TODO Auto-generated method stub

		String query = "insert into party values(?,?,?,?)";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, p.getPartyId());
		prepareStatement.setString(2, p.getPartyName());
		prepareStatement.setString(3, p.getCandidateName());
		prepareStatement.setInt(4, p.getNoOfVotes());
		int j = prepareStatement.executeUpdate();
		
		String query1 = "insert into partyCredentials values(?,?)";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query1);
		prepareStatement.setString(1, p.getpCredentials().getPartyId());
		prepareStatement.setString(2, p.getpCredentials().getPartyPassword());
		int k = prepareStatement.executeUpdate();

		return j;

	}

	@Override
	public int deleteParty(String partyId) throws Exception {
		// TODO Auto-generated method stub
		String query = "delete from partyCredentials where partyId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, partyId);
		prepareStatement.executeUpdate();
		
		String query1 = "delete from party where partyId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query1);
		prepareStatement.setString(1, partyId);
		int j = prepareStatement.executeUpdate();
		return j;

	}

	@Override
	public int deleteVoters(String userId) throws Exception {
		// TODO Auto-generated method stub
		
		
		String query = "delete from voterDetails where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, userId);
		int j = prepareStatement.executeUpdate();
		
		
		String query1 = "delete from voterCredentials where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query1);
		prepareStatement.setString(1, userId);
		prepareStatement.executeUpdate();
		return j;
	}

	@Override
	public void declareResults() throws Exception {

		String query = "update admin set hasDeclared=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setInt(1, 1);
		prepareStatement.executeUpdate();

	}

	public List<Voters> showAllVoters() throws Exception{
		List<Voters> voterList = new ArrayList<>();
		
		String query = "select * from voterDetails ";
		Statement s = new ConnectionClass().connection().createStatement();
		ResultSet rs = s.executeQuery(query);
		while(rs.next())
		{ Voters voter= new Voters();
			voter.setVoterId(rs.getString(1));
			voter.setVoterName(rs.getString(2));
			voter.setGender(rs.getString(3));
			voter.setDob(rs.getDate(4));
			voter.setHasVoted(rs.getInt(5));
		
			VotersCredentials vc = new VotersCredentials();
			vc.setUserId(rs.getString(6));
			voter.setVoterCredentials(vc);
			voterList.add(voter);
		}
		return voterList;
	}

}
