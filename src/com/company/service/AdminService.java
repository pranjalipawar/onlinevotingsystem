package com.company.service;

import java.util.List;

import com.company.entity.Party;
import com.company.entity.Voters;
import com.company.persistence.AdminDaoImpl;

public class AdminService {

	public int adminLogin(String adminId, String adminPassword) throws Exception {
		return 0;
	}

	public int addParty(Party p) throws Exception {
		return new AdminDaoImpl().addParty(p);
	}

	public int deleteParty(String partyId) throws Exception {

		return new AdminDaoImpl().deleteParty(partyId);

	}

	public int deleteVoters(String userId) throws Exception {

		return new AdminDaoImpl().deleteVoters(userId);
	}

	public void declareResults() throws Exception {

	 new AdminDaoImpl().declareResults();
	}
	
	public List<Party> showAllParty() throws Exception{
		return new AdminDaoImpl().showAllParty();
	}
	
	public List<Voters> showAllVoters() throws Exception{
		return new AdminDaoImpl().showAllVoters();
	}
}
