package com.company.persistence;

import com.company.entity.Party;
import com.company.entity.Voters;

public interface UserDao {

	public int voteToAParty(String partyId,String userId) throws Exception;
	public int registerUserDetails(Voters voter) throws Exception;
	public int loginUser(String userId,String password) throws Exception;
	public int deleteVoters(String voterId) throws Exception;
	public int updateVoters(String voterId,String password) throws Exception;
	public String showResults() throws Exception;
}
