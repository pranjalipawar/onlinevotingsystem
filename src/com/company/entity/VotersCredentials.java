package com.company.entity;

public class VotersCredentials {
	
	private String userId;
	private String password;
	
	public VotersCredentials() {
		
	}

	public VotersCredentials(String userId, String password) {
		
		this.userId = userId;
		this.password = password;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "VotersCredentials [userId=" + userId + ", password=" + password + "]";
	}
	

}
