package com.company.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

import com.company.entity.Voters;
import com.company.ui.ConnectionClass;

public class UserDaoImpl implements UserDao {
	static PreparedStatement prepareStatement = null;
	static ResultSet rs = null;
	@Override
	public int voteToAParty(String partyId,String userId) throws Exception {
		
		String query;
		
		
		query = "select partyId from party where partyId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, partyId);
	    rs = prepareStatement.executeQuery();
	    if(rs.next()) {
	    	
	    }
	    else {
	    	return -2;
	    }
		
		query = "select hasVoted from voterDetails where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, userId);
	    rs = prepareStatement.executeQuery();
	    
	    if(rs.next()) {
	    	if(rs.getInt(1)==1) {
	    		return -1;
	    	}
	    }
		
		query = "update party set noOfVotes =noOfVotes+1 where partyId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, partyId);
		int j = prepareStatement.executeUpdate();
		
		query = "update voterDetails set hasVoted =1 where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, userId);
		 j = prepareStatement.executeUpdate();
		
		return j;
		}

	@Override
	public int registerUserDetails(Voters voter) throws Exception {
		String query1= "insert into voterCredentials values(?,?)";
		   prepareStatement = new ConnectionClass().connection().prepareStatement(query1);
		   prepareStatement.setString(1, voter.getVoterCredentials().getUserId());
		   prepareStatement.setString(2, voter.getVoterCredentials().getPassword());
		   int k = prepareStatement.executeUpdate();
		 String query = "insert into voterDetails values(?,?,?,?,?,?)";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, voter.getVoterId());
		prepareStatement.setString(2, voter.getVoterName());
		prepareStatement.setString(3, voter.getGender());
		prepareStatement.setDate(4, voter.getDob());
		prepareStatement.setInt(5, 0);
		prepareStatement.setString(6, voter.getVoterCredentials().getUserId());
	   int j = prepareStatement.executeUpdate();
		
	   
	
	return j;
	}

	@Override
	public int loginUser(String userId, String password) throws Exception {
		String query = "select * from voterCredentials where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1,userId);
		 
		rs = prepareStatement.executeQuery();
		if(rs.next()) {
			if(password.equals(rs.getString(2)))
				return 1;
		}
		
		return 0;
	}

	@Override
	public int deleteVoters(String userId) throws Exception {
		String query = "delete from voterDetails where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, userId);
		int j = prepareStatement.executeUpdate();
		
		 query = "delete from voterCredentials where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, userId);
	    j = prepareStatement.executeUpdate();
		
		return j;
	}

	@Override
	public int updateVoters(String userId, String password) throws Exception {
		String query = "update voterCredentials set vpassword =? where userId=?";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		prepareStatement.setString(1, password);
		prepareStatement.setString(2, userId );
		int j = prepareStatement.executeUpdate();
		return j;
		
	}

	@Override
	public String showResults() throws Exception {
		String query = "select hasdeclared from admin";
		prepareStatement = new ConnectionClass().connection().prepareStatement(query);
		rs = prepareStatement.executeQuery();
		if(rs.next()) {
			
			if(rs.getInt(1)==1) {
				
				String query1 = "select partyName,noOfVotes from party where noOfVotes=(select max(noOfVotes) from party) limit 2";
				
				
				prepareStatement = new ConnectionClass().connection().prepareStatement(query1);
				rs = prepareStatement.executeQuery();
				
				int noOfvotes = 0;
				int noOfvotes1 = 0;
				String name1 = "";
				String name2 = "";
				
				int j = 0;
				while(rs.next()) {
					
					if(j==0) {
					noOfvotes = rs.getInt(2);
					name1 = rs.getString(1);
					
					System.out.println(noOfvotes+name1);
					}
					else {
						noOfvotes1 = rs.getInt(2);
						name2 = rs.getString(1);
						System.out.println(noOfvotes1+name2);
					}
					j++;
//					System.out.println(rs.getString(1)+" won the election with "+ noOfvotes +" Total number of votes");
//					
//					
//					return rs.getString(1)+" won the election with "+rs.getInt(2)+" Total number of votes";
				}
				if(noOfvotes == noOfvotes1) {
				
					return "Election Tied. Admin Will Restart the Election";
				}
				
				else {
					return name1+" won the election with "+noOfvotes+" Total number of votes";
				}
			}
				
		}
		
		return "Result Not Declared";
	}
	

}
