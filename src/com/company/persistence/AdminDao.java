package com.company.persistence;

import java.util.List;

import com.company.entity.Party;
import com.company.entity.Voters;

interface AdminDao {
	
	
	public int adminLogin(String adminId,String adminPassword) throws Exception;
	public void showVotersDetails() throws Exception;
	public int addParty(Party p) throws Exception;
    public int deleteParty(String partyId) throws Exception;
	public int deleteVoters(String voterId) throws Exception;
	public List<Party> showAllParty() throws Exception;
	public List<Voters> showAllVoters() throws Exception;
	
	public void declareResults() throws Exception;
}
