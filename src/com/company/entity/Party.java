package com.company.entity;

public class Party {

	private String partyId;
	private String partyName;
	private String candidateName;
	private int noOfVotes;
	PartyCredential pCredentials;
	public Party() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Party(String partyId, String partyName, String candidateName, int noOfVotes, PartyCredential pCredentials) {
		super();
		this.partyId = partyId;
		this.partyName = partyName;
		this.candidateName = candidateName;
		this.noOfVotes = noOfVotes;
		this.pCredentials = pCredentials;
	}
	
	public Party(String partyId, String partyName, String candidateName, int noOfVotes) {
		super();
		this.partyId = partyId;
		this.partyName = partyName;
		this.candidateName = candidateName;
		this.noOfVotes = noOfVotes;
	}
	
	
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getCandidateName() {
		return candidateName;
	}
	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}
	public int getNoOfVotes() {
		return noOfVotes;
	}
	public void setNoOfVotes(int noOfVotes) {
		this.noOfVotes = noOfVotes;
	}
	public PartyCredential getpCredentials() {
		return pCredentials;
	}
	public void setpCredentials(PartyCredential pCredentials) {
		this.pCredentials = pCredentials;
	}
	@Override
	public String toString() {
		return "Party [partyId=" + partyId + ", partyName=" + partyName + ", candidateName=" + candidateName
				+ ", noOfVotes=" + noOfVotes+" ]" ;
	}
	
	
	
	
}
