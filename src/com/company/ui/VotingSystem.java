package com.company.ui;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.company.entity.Party;
import com.company.entity.PartyCredential;
import com.company.entity.Voters;
import com.company.entity.VotersCredentials;
import com.company.persistence.PartyDaoImpl;
import com.company.persistence.UserDaoImpl;
import com.company.service.AdminService;
import com.company.service.PartyService;
import com.company.service.UserService;

public class VotingSystem {

	static Scanner sc = new Scanner(System.in);
	static boolean adminLoggedIn = false;
	static boolean userloggedin = false;
	static boolean partyloggedin = false;
	static String globalUserId = "";
	static boolean toTerminate = true;
	static int hasStarted = 0;
	static int hasdeclared = 0;
	static Statement st = null;
	static ResultSet rs = null;
	static boolean loginpage = true;

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("\n--------------------Welcome to Online Voting System--------------------------\n");
		while (toTerminate) {
			try {

				st = new ConnectionClass().connection().createStatement();
				rs = st.executeQuery("select hasStarted,hasdeclared from admin");
				if (rs.next()) {
					hasStarted = rs.getInt(1);
					hasdeclared = rs.getInt(2);
					
					
				}

				do {

					System.out.println("You Want To Login as \n1.Admin\n2.User\n3.Party Member\n4.Exit\n");

					int choice = sc.nextInt();
					sc.nextLine();

					if (choice == 1) {
						System.out.println("Enter AdminId\n");
						String adminId = sc.next();
						System.out.println("Enter Admin Password\n");
						String pass = sc.next();
						st = new ConnectionClass().connection().createStatement();
						String query = "select * from admin";
						rs = st.executeQuery(query);

						if (rs.next()) {

							if (rs.getString(1).equals(adminId) && rs.getString(2).equals(pass)) {

								System.out.println("Admin Login Successfully\n");

								adminLoggedIn = true;

								if (adminLoggedIn == true) {
									do {
										try {
											System.out.println(
													"1.Add Party\n2.Delete Party\n3.Deleter User\n4.Declare Results\n5.Start Voting\n6.Show All VoterList \n7.Show all PartyList\n8.Logout\n");
											int choice3 = sc.nextInt();
											sc.nextLine();
											if (choice3 == 1) {
												int success = addPartyForVote();
												if (success == 1) {
													System.out.println("Party Added Successfully\n");
												} else {
													System.out.println("Something Went Wrong\n");
												}
											} else if (choice3 == 2) {
												
												showAllPartyDetails();
												int k = deleteparty();

												if (k == 1)
													System.out.println("Party Removed\n");
												else {
													System.out.println("Party doesn't exists\n");
												}
											} else if (choice3 == 3) {
												//3rd Change
												showVoterDetails();
												System.out.println("Enter User Id for Deletion\n");
												String userId = sc.next();
												globalUserId = userId;
												int k = deleteUser();
												if (k == 1)
													System.out.println("User Removed\n");
												else {
													System.out.println("User doesn't exists\n");
												}
											} else if (choice3 == 4) {

												//2nd Change
												if(hasdeclared==1) {
													
													System.out.println("Already Declared\n");
												}
												
												else if(hasStarted==1) {
												st.executeUpdate("update admin set hasStarted = 0");
												st.executeUpdate("update admin set hasdeclared = 1");
												st.executeUpdate(
														"update admin set partyWon=(select partyName from party where noOfVotes=(select Max(noOfVotes) from party limit 1) limit 1)");
												st.executeUpdate("update voterDetails set hasVoted =0");
												hasStarted = 0;
												hasdeclared = 1;
												
												System.out.println("Result Declared\n");
												}
												else {
													System.out.println("First Start the Election's\n");
												}
											} else if (choice3 == 5) {

												if (hasStarted == 1) {
													System.out.println("Election is already in process....\n");
												}

												else {
													st.executeUpdate("update admin set hasdeclared = 0");
													st.executeUpdate("update admin set partyWon = null");
													st.executeUpdate("update party set noOfVotes=0");
													int j = st.executeUpdate("update admin set hasStarted = 1");
													hasStarted = 1;
													hasdeclared=0;
													System.out.println("Election Started\n");
												}

											} else if (choice3 == 6) {

												showVoterDetails();
											} else if (choice3 == 7) {
												showAllPartyDetails();

											} else if (choice3 == 8) {
												System.out.println("Logged out Succesfully\n");
												adminLoggedIn = false;
												break;
											}
										} catch (SQLException e) {
											String bad_input = sc.next();
											System.out.println("Party already participated\n");
											continue;
										} catch (InputMismatchException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;

										} catch (NumberFormatException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;
										} catch (IllegalArgumentException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;
										}
									} while (true);
								}
							} else {
								System.out.println("Wrong Credentials\n");

							}
						}

					} else if (choice == 2) {

						do {
							try {
								if (userloggedin == true) {
									System.out.println(
											"1.Want To Vote\n2.Change Your Password\n3.Delete your Account \n4.See Results\n5.Logout\n");
									int ch1 = sc.nextInt();
									sc.nextLine();
									if (ch1 == 1) {
										if (hasStarted == 1) {
											showAllPartyDetails();
											
											int k = voteToAParty(globalUserId);

											if (k == -1) {
												System.out.println("Already Voted\n");
											} else if (k == 1) {

												System.out.println("Voting Done Successfully \n Result will Out Soon\n");
											}
											//updated
											else if(k==-2) {
												System.out.println("PartyId not Exist\n");
											}
											else {
												System.out.println("Something went wrong\n");
											}
										} 
										else if(hasdeclared ==1) {
											System.out.println("Result Declared \nuse Show Result Option\n");
										}
										
										else {
											System.out.println("Voting haven't started yet\n");
										}
									} else if (ch1 == 2) {
										int k = updateUser();
										if (k == 1) {
											System.out.println("Your Password has been changed\n");
										}

									} else if (ch1 == 3) {
										
										int k = deleteUser();
										if (k == 1)
											System.out.println("Account Deleted\n");
										userloggedin = false;
									} else if (ch1 == 4) {
										if (hasdeclared == 1) {
											System.out.println(showResults());
										}
										else if(hasStarted==1) {
											System.out.println("Election going on....\n");
										}
										else {
											
											System.out.println("Election haven't Started Yet\n");
										}

									} else if (ch1 == 5) {
										
										globalUserId = "";
										userloggedin = false;
										System.out.println("Logged out Succesfully\n");
										break;
									}
								}

								else {
									try {
										System.out.println("1.Login \n2.Register\n3.Exit\n");

										int choice1 = sc.nextInt();
										//sc.nextLine();
										switch (choice1) {

										case 1:
											int n = logintoVoterCredentials();
											if (n == 1) {
												userloggedin = true;
											} else {
												System.out.println("Wrong Credentials\n");
											}
											break;
										case 2:

											int j = registerVoterDetails();
											if (j == 1) {
												userloggedin = true;
												System.out.println("Register Succesfully");
												System.out.println("You Want To Vote Right now \n1.Yes \n2.No\n");
												int choice2 = sc.nextInt();
												sc.nextLine();

												if (choice2 == 1) {
													if ( hasStarted == 1) {
														int k = voteToAParty(globalUserId);
														if (k == 1) {
															System.out.println(
																	"Voting Done Successfully \n Result will Out Soon\n");
														}
														else if(k==-2) {
															System.out.println("PartyId not Exist\n");
														}
														else {
															System.out.println("Something went wrong\n");
														}
													}
													
													else {
														System.out.println("Election haven't Started Yet\n");
													}
												}

											} else {
												System.out.println("Something went wrong\n");
											}
											break;

										case 3:loginpage=false;
										      break;
										}
									} catch (SQLException e) {
										String bad_input = sc.next();
										System.out.println("User already registered\n");
										continue;
									} catch (InputMismatchException e) {
										String bad_input = sc.next();
										System.out.println("Wrong Input\n");
										continue;

									} catch (IllegalArgumentException e) {
										String bad_input = sc.next();
										System.out.println("Wrong Input\n");
										continue;
									}
								}
							}  catch (InputMismatchException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;

							} catch (NumberFormatException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;
							} catch (IllegalArgumentException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;
							}
						} while (loginpage);
					}

					else if (choice == 3) {

						do {

							try {
								System.out.println("Login to See Your Details\n");
								System.out.println("Enter party Id\n");
								String partyId = sc.nextLine();

								partyloggedin = partyLogin(partyId);

								if (partyloggedin) {
									do {
										try {
											System.out.println("1.Party Details\n2.See Results\n3.Logout\n");
											int ch2 = sc.nextInt();

											if (ch2 == 1) {
												showPartyDetails(partyId);
											} else if (ch2 == 2) {

												if (hasdeclared == 1 && hasStarted == 0) {
													System.out.println(showResults());
												}
												else if(hasStarted==1) {
													System.out.println("Election going on....\n");
												}
												else {
													System.out.println("Election haven't Started Yet\n");
												}

											} else {
												partyloggedin = false;
												System.out.println("Logged out Succesfully\n");
												break;
											}
										} 
										catch (SQLException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;
										} 
										catch (InputMismatchException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;

										} catch (NumberFormatException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;
										} catch (IllegalArgumentException e) {
											String bad_input = sc.next();
											System.out.println("Wrong Input\n");
											continue;
										}

									} while (true);
								} else {
									System.out.println("Wrong Credentials\n");
								}
						} 
							catch (SQLException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;
							} 
							catch (InputMismatchException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;

							} catch (NumberFormatException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;
							} catch (IllegalArgumentException e) {
								String bad_input = sc.next();
								System.out.println("Wrong Input\n");
								continue;
							}
						} while (partyloggedin);
					} else if (choice == 4) {
						System.exit(0);

					}

				} while (true);

		} 
				catch (SQLException e) {
				String bad_input = sc.next();
				System.out.println("Wrong Input\n");
				continue;
			} 
			catch (InputMismatchException e) {
				String bad_input = sc.next();
				System.out.println("Wrong Input\n");
				continue;

			} catch (NumberFormatException e) {
				String bad_input = sc.next();
				System.out.println("Wrong Input\n");
				continue;
			} catch (IllegalArgumentException e) {
				String bad_input = sc.next();
				System.out.println("Wrong Input\n");
				continue;
			}
		}

	}

	/* All Functions */
	private static void showAllPartyDetails() throws Exception {
		// TODO Auto-generated method stub
		List<Party> partyList = new ArrayList<>();
		partyList = new AdminService().showAllParty();
		for (Party party : partyList) {
			System.out.println("PartyId: " + party.getPartyId() + " | Party Name:" + party.getPartyName());
			System.out
					.println("Candidate Name: " + party.getCandidateName() + " | No Of Votes:" + party.getNoOfVotes());
			System.out.println("------------------------------------------------------");
		}

	}

	private static void showVoterDetails() throws Exception {

		List<Voters> voterList = new AdminService().showAllVoters();
		for (Voters v : voterList) {
			System.out.println("Voter UserID: " + v.getVoterCredentials().getUserId());
			System.out.println("Voter ID: " + v.getVoterId());
			System.out.println("Voter Name: " + v.getVoterName());
			System.out.println("Gender: " + v.getGender());
			System.out.println("Date Of Birth: " + v.getDob());
			System.out.println("Has the voter voted: " + v.getHasVoted());
			System.out.println("--------------------------------------------------");
		}

		

	}

	private static boolean partyLogin(String partyId) throws Exception {

		System.out.println("Enter Party Password");
		String partyPassword = sc.next();
		sc.nextLine();
		return new PartyService().partyLogin(partyId, partyPassword);
	}

	private static void showPartyDetails(String partyId) throws Exception {
		Party p = new PartyService().showPartyDetail(partyId);
		System.out.println("PartyId: " + p.getPartyId() + " | Party Name:" + p.getPartyName());
		System.out.println("Candidate Name: " + p.getCandidateName() + " | No Of Votes:" + p.getNoOfVotes());
		System.out.println("------------------------------------------------------");
	}

	private static int deleteparty() throws Exception {

		System.out.println("Enter Party Id You want to delete");
		String partyId = sc.next();
		return new AdminService().deleteParty(partyId);
	}

	private static int deleteUser() throws Exception {

		return new AdminService().deleteVoters(globalUserId);
	}

	public static int updateUser() throws Exception {

		
		System.out.println("Enter New Password");
		String newPassword = sc.next();
		return new UserService().updateVoters(globalUserId, newPassword);
	}

	private static int addPartyForVote() throws Exception {

		System.out.println("Enter Party Details\n");
		System.out.println("---------------------------\n");
		System.out.println("Enter Party Id on Which Voter will Vote\n");
		String partyId = sc.next();
		System.out.println("Enter Party Name");
		String partyName = sc.next();
		System.out.println("Enter Candidate Name");
		String cName = sc.next();
		System.out.println("Enter Password For Party Login");
		String password = sc.nextLine();
		//4th change
		PartyCredential pc = new PartyCredential(partyId, password);
		Party p = new Party(partyId, partyName, cName, 0, pc);
		return new AdminService().addParty(p);

	}

	public static boolean hasVotingStarted() throws Exception {
		Statement st = new ConnectionClass().connection().createStatement();
		String query = "select hasStarted from admin";
		ResultSet rs = st.executeQuery(query);
		if (rs.next()) {
			if (rs.getInt(1) == 0)
				return false;
		}

		return true;
	}

	public static int registerVoterDetails() throws Exception {

		System.out.println("Please Fill your Details");
		System.out.println("----------------------------");
		System.out.println("Enter Your Voter Card No.(must be 10digit)");
		String voterId = sc.next();
		System.out.println("Enter Your Name");
		String name = sc.next();
		System.out.println("Enter Your Gender (M/F)");
		String gender = sc.next();
		System.out.println("Enter Your Date of Birth in YYYY-MM-DD Format");
		String dob = sc.next();
		Date d = Date.valueOf(dob);
		System.out.println("Enter User Id( must be alphanumeric)");
		String userId = sc.next();
		System.out.println("Enter your Password");
		String password = sc.next();
		//5th change
		while(password.length()<8) {
			System.out.println("ReEnter Your Password(Passwoed Must be of 8 digit)");
			password = sc.next();
			if(password.length()>8) {
				break;
			}
		}
		VotersCredentials vc = new VotersCredentials(userId, password);
		Voters voter = new Voters(voterId, name, gender, d, 0, vc);
		int update = new UserService().registerUserDetails(voter);
		//1st Change
		
        if(update==1) {
        	globalUserId = userId;
        }
		return update;
	}

	public static int voteToAParty(String userId) throws Exception {

		System.out.println("Enter Party Id to Vote");
		String partyId = sc.next();
		return new UserService().voteToAParty(partyId, userId);
	}

	private static int logintoVoterCredentials() throws Exception {

		System.out.println("Enter UserId\n");
		String userId = sc.next();
		System.out.println("Enter Your Password\n");
		String password = sc.next();

		int n = new UserService().loginUser(userId, password);
		if (n == 1) {
			globalUserId = userId;
		}
		return n;
	}

	public static String showResults() throws Exception {
		
		return new PartyDaoImpl().showResults();
	}

}
