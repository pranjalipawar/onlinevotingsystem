package com.company.entity;

import java.sql.Date;

public class Voters {
	
	private String voterId;
	private String voterName;
	private String gender;
	private Date dob;
	private int hasVoted;
	private VotersCredentials voterCredentials;
	public Voters() {
		
		// TODO Auto-generated constructor stub
	}
	public Voters(String voterId, String voterName, String gender, Date dob, int hasVoted,
			VotersCredentials voterCredentials) {
		super();
		this.voterId = voterId;
		this.voterName = voterName;
		this.gender = gender;
		this.dob = dob;
		this.hasVoted = hasVoted;
		this.voterCredentials = voterCredentials;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getVoterName() {
		return voterName;
	}
	public void setVoterName(String voterName) {
		this.voterName = voterName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public int getHasVoted() {
		return hasVoted;
	}
	public void setHasVoted(int hasVoted) {
		this.hasVoted = hasVoted;
	}
	public VotersCredentials getVoterCredentials() {
		return voterCredentials;
	}
	public void setVoterCredentials(VotersCredentials voterCredentials) {
		this.voterCredentials = voterCredentials;
	}
	@Override
	public String toString() {
		return "Voters [voterId=" + voterId + ", voterName=" + voterName + ", gender=" + gender + ", dob=" + dob
				+ ", hasVoted=" + hasVoted + ", userId=" + voterCredentials.getUserId() + "]";
	}
	
}
